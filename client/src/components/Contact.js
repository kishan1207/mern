import React from 'react'

const Contact = () => {
  return (
    <>
      <h1 style={{ textAlign: 'center' }}>Contact Us</h1>
      <div className="card shadow-sm border-0 px-3 rounded-2 mb-3 py-4 mx-auto mt-5 bg-light">
        <div className="card-body">
          <form action="/" onSubmit={(e) => this.handleSubmit(e)} encType="multipart/form-data" autoComplete="off">
            <div className="form-group">
              <label className="mb-0">Your name<span className="text-danger">*</span></label>
              <input name="name" type="text" className="form-control" placeholder="Name" />
            </div>
            <div className="form-group">
              <label className="mb-0">Your email<span className="text-danger">*</span></label>
              <input name="email" type="email" className="form-control" placeholder="Email" />

            </div>
            <div className="form-group">
              <label className="mb-0">Your contact number (Optional)</label>
              <input name="contact" type="text" className="form-control" placeholder="Contact" />
            </div>
            <div className="form-group">
              <label className="mb-0">Message<span className="text-danger">*</span></label>
              <textarea name="message" type="text" className="form-control" placeholder="Message" />

            </div>
            <p className="text-center mb-0"><input type="submit" className="btn btn-primary btn-lg w-100 text-uppercase" value="Submit Now" /></p>
          </form>

        </div>
      </div>
    </>
  )
}

export default Contact
