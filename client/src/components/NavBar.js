import React from 'react'
import { Navbar, Nav, Container } from 'react-bootstrap';
import { NavLink } from "react-router-dom";

const NavBar = () => {
    return (
        <>
            <Navbar bg="light" variant="light">
                <Container>
                    <Navbar.Brand ><NavLink className='link' className='NavLink' to='/'>MERN-Project</NavLink></Navbar.Brand>
                    <Nav className="mr-auto">
                        <NavLink to='/' className='NavLink'>Home</NavLink>
                        <NavLink to='/about' className='NavLink'>About</NavLink>
                        <NavLink to='/contact' className='NavLink'>Contact</NavLink>
                        <NavLink to='/login' className='NavLink'>Login</NavLink>
                        <NavLink to='/signup' className='NavLink'>Signup</NavLink>
                    </Nav>
                </Container>
            </Navbar>
        </>
    )
}

export default NavBar
