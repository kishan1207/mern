import React, { useState } from 'react'
import { Link,useHistory } from "react-router-dom";
import axios from "axios";

const Signup = () => {
    const history = useHistory()
    const [user, setUser] = useState({
        name: '',
        email: '',
        phone: '',
        work: '',
        password: '',
        cpassword: '',
    })
    const HandleInputs = (e) => {
        let name, value;
        name = e.target.name;
        value = e.target.value;

        setUser({ ...user, [name]: value })
    }

    const handleSubmit = async (e) =>{
        console.log("kishan");
        e.preventDefault();
        const {  name,email,phone,work,password,cpassword} = user;
       const res = await axios.post("http://localhost:8000/signup", {
            name,email,phone,work,password,cpassword
          })

          if(res.status == 201 && res ){
            window.alert('Registration successfully')
            history.push('/login')
          }else{
            window.alert('Invalid Registration')
          }
    }

    return (
        <div className='container mt-4'>
            <form method='POST'>
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label>Your name</label>
                    <input type="text" className="form-control" name="name" value={user.name} onChange={HandleInputs} placeholder="Enter your name" />
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input type="email" className="form-control" name="email" value={user.email} onChange={HandleInputs} placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Phone number</label>
                    <input type="number" className="form-control" name="phone" value={user.phone} onChange={HandleInputs} placeholder="Enter your phone number" />
                </div>

                <div className="form-group">
                    <label>Your Profession</label>
                    <input type="text" className="form-control" name="work" value={user.work} onChange={HandleInputs} placeholder="Your Profession" />
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" value={user.password} onChange={HandleInputs} placeholder="Enter password" />
                </div>

                <div className="form-group">
                    <label>Confirm Password</label>
                    <input type="password" className="form-control" name="cpassword" value={user.cpassword} onChange={HandleInputs} placeholder="Enter password again" />
                </div>

                <button type="submit" 
                className="btn btn-dark btn-lg btn-block"
                onClick={handleSubmit}
                >Register</button>

                <p className="forgot-password text-right">
                    Already registered <Link to='/login'>log in?</Link>
                </p>
            </form>

        </div>
    )
}

export default Signup
