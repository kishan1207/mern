import React,{useState} from 'react'
import { Link,useHistory } from "react-router-dom";
import axios from "axios";
import cookie from 'js-cookie';

const Login = () => {
    const [email, setemail] = useState('')
    const [password, setpassword] = useState('')
    const history = useHistory();

    const handleSubmit = async (e) =>{
        e.preventDefault();

        const res = await axios.post("http://localhost:8000/signin", {
           email,password
          })

          if(res.status != 200 || !res ){
            window.alert('Invalid credintial')
          }else{
          cookie.remove('token')
          cookie.set('token',res.data.token)
          
            window.alert('login successfully')
            history.push('/')
          }
    }

    return (
        <>
            <div className='container mt-5'>
                <form method='POST'>

                    <h3>Log in</h3>

                    <div className="form-group">
                        <label>Email</label>
                        <input type="email" 
                        value={email}
                        onChange={(e)=>setemail(e.target.value)}
                        className="form-control"
                         placeholder="Enter email" />
                    </div>

                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" 
                         value={password}
                         onChange={(e)=>setpassword(e.target.value)}
                        className="form-control" 
                        placeholder="Enter password" />
                    </div>

                    <button type="submit" 
                    onClick={handleSubmit}
                    className="btn btn-dark btn-lg btn-block">Sign in</button>

                    <p className="forgot-password text-right">
                        <Link to='/signup'>Create Account</Link>
                    </p>
                </form>
            </div>
        </>
    )
}

export default Login
