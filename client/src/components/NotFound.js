import React from 'react'

const NotFound = () => {
    return (
        <>
        <h1 style={{textAlign:'center',textDecoration:'bold'}}>404 Page Not Found</h1>
        </>
    )
}

export default NotFound
