import React ,{useEffect,useState}from 'react'
import {Card,Button } from 'react-bootstrap';
import aboutUs from '../Images/AboutUs_Logo.png'
import axios from 'axios';
import { useHistory } from "react-router-dom";

const About = () => {
    const history = useHistory();
const [userData, setuserData] = useState(null)
    useEffect(() => {
     callAboutPage();
    }, [])


    const callAboutPage = async () =>{

        try {
        const res = await axios.get("http://localhost:8000/about")

          if(res.status != 200 || !res ){
            window.alert('Invalid user data')
          }else{
            setuserData(res)
          }
        }catch (error) {
            console.log(error);
            history.push('/')
        }
    }
console.log(userData);
    return (
        <>
            <Card style={{ width: '18rem' ,marginTop:'50px',marginLeft:'80vh'}}>
                <Card.Img variant="top" src={aboutUs} />
                <Card.Body>
                    <Card.Title>kishan khadela</Card.Title>
                    <Card.Text>
                       this is my first MERN stack Project
                    </Card.Text>
                    <Button >go to Home</Button>
                </Card.Body>
            </Card>
        </>
    )
}

export default About
