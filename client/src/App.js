import './App.css';
import { Switch, Route } from "react-router-dom";
import NavBar from './components/NavBar'
import Home from './components/Home'
import About from './components/About'
import Contact from './components/Contact'
import Login from './components/Login'
import Signup from './components/Signup'
import NotFound from './components/NotFound'


function App() {
  return (
    <>
      <NavBar />
      <Switch>

        <Route path='/login'>
          <Login />
        </Route>

        <Route path='/about'>
          <About />
        </Route>

        <Route path='/signup'>
          <Signup />
        </Route>

        <Route path='/contact'>
          <Contact />
        </Route>

        <Route exact path='/'>
          <Home />
        </Route>

        <Route >
          <NotFound />
        </Route>

      </Switch>
    </>
  );
}

export default App;
