const User = require('../model/userSchema')
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv').config();


const authenticate = async (req,res,next) =>{
    try {
        const {SECRET_KEY} = process.env;
        const token = req.cookie.jwtoken;
        console.log("kishan",token);
        const verifyToken = jwt.verify(token,SECRET_KEY);

        const rootUser = await User.findOne({_id:verifyToken._id,"token.token":token}); 
        if(!rootUser){
            throw new Error("user not found")
        }

        req.token = token;
        req.rootUser = rootUser;
        req.userID = rootUser._id;

        next();


    } catch (error) {
        res.status(401).send('Unauthorized : No token provided')
        console.log(error);
    }


}

module.exports = authenticate