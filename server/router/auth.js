const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const authenticate = require('../middleware/authenticate')



const User = require('../model/userSchema')

router.get('/', (req, res) => {
    res.send('hello from the server')
})

router.post('/signup', async (req, res) => {
    try {

        const { name, email, phone, work, password, cpassword } = req.body;

        if (!name || !email || !phone || !work || !password || !cpassword) {
            res.status(422).json({ error: 'please fill the data' })
        }
        const userExist = await User.findOne({ email: email });

        if (userExist) {
            res.status(422).json({ error: 'email already exist' })
        } else if (password != cpassword) {
            res.status(422).json({ error: 'password not match' })
        }
        else {
            const user = new User({ name, email, phone, work, password, cpassword });

            await user.save();
            res.status(201).json({ ...req.body, meassage: "user registration successfully" });
        }

    } catch (error) {
        console.log(error)
    }

})

router.post('/signin', async (req, res) => {
    try {

        const { email, password } = req.body;
        if (!email || !password) {
            return res.status(400).json({ error: "please fill the data" })
        }
        const userLogin = await User.findOne({ email: email });

        if (userLogin) {
            const isMatch = await bcrypt.compare(password, userLogin.password);

            if (!isMatch) {
                res.status(400).json({ error: "Invalid credintial" })
            } else {
                const token = await userLogin.generateAuthToken();
            
                res.json({
                    token:token,
                    status:200,
                    message: "Logged in successfully 😊 👌" });
            }

        } else {
            res.status(400).json({ error: "Invalid credintial" })
        }

    } catch (err) {
        console.log(err);

    }
});

router.get('/about', authenticate, (req, res) => {
    console.log("pabout page");
    res.send(req.rootUser)
})


module.exports = router;