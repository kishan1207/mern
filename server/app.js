
const dotenv = require('dotenv').config();
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');


const app = express();

const {PORT} = process.env

// CORS error
app.use(cors());
app.use(cookieParser());

// database connection

require('./db/con')
app.use(express.json())

// routing
app.use(require('./router/auth'))

app.listen(PORT,()=>{
    console.log(`server starting port on ${PORT}`);
})