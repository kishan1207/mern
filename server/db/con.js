const mongoose = require('mongoose');
const dotenv = require('dotenv').config();

const {DATABASE} = process.env


mongoose.connect(DATABASE,{
    useNewUrlParser: true, 
    useUnifiedTopology: true 
}).then(()=> {
    console.log('database connect successfully')
}).catch((err)=> console.log(err))